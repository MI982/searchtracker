package searchtracker.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import searchtracker.configurations.SecurityConfiguration;
import searchtracker.exceptions.UserHasNoQueriesException;
import searchtracker.models.FindUserQueriesModel;
import searchtracker.models.UserModel;
import searchtracker.services.TokenService;
import searchtracker.services.UserService;
import searchtracker.validators.LoginUserValidator;

@WebMvcTest(RestUserController.class)
@Import(SecurityConfiguration.class)
public class RestUserControllerTest {
	
	private final String GET_USER_WITH_QUERIES_URI = "/user/finduser";
	private final String TEST_USERNAME = "testusername";
	
	@Autowired
	private MockMvc mockedMvc;
	
	@Autowired
	private ObjectMapper mapper;
	
	@MockBean
	private UserService mockedUserService;
	
	@MockBean
	private TokenService mockedTokenService;
	
	@MockBean
	private LoginUserValidator mockedLoginUserValidator;
	
	
	@Test
	public void secTestOne() throws Exception {
		
		
		MvcResult result = mockedMvc.perform(get("/user/securitytest1"))
										.andExpect(status().is2xxSuccessful())
										.andReturn();
		
		String stringContent = result.getResponse().getContentAsString();
		HashMap<String, String> body = mapper.readValue(stringContent, HashMap.class);
		assertThat(body.get("message")).isEqualTo("Security 1 operational!");
		
	}
	
	@Test
	@WithMockUser(username = "testusername", authorities = "ROLE_ADMIN")
	@DisplayName("Get user with queries, throws UserHasNoQueriesException")
	public void getUserThrows() throws JsonProcessingException, Exception {
		
		FindUserQueriesModel testModel = new FindUserQueriesModel();
		testModel.setUsername(TEST_USERNAME);
		
		
		when(mockedUserService.getUserWithQueries(any(), any(), any(), any(), any()))
			.thenThrow(new UserHasNoQueriesException(TEST_USERNAME));
		
		
		MvcResult result = mockedMvc.perform(
									post(GET_USER_WITH_QUERIES_URI)
									.accept(MediaType.APPLICATION_JSON)
									.content(mapper.writeValueAsString(testModel))
									.contentType(MediaType.APPLICATION_JSON)
									)
									.andExpect(status().isNotFound())
									.andReturn()
									;
		
		
		String content = result.getResponse().getContentAsString();
		
		HashMap<String,String> body = mapper.readValue(content, HashMap.class);
		
		assertThat(body.get("message"))
					.isEqualTo(new StringBuilder(TEST_USERNAME).append(" has no queries!").toString());
	}
	
	@Test
	@WithMockUser(username = TEST_USERNAME, authorities = "ROLE_ADMIN")
	@DisplayName("Given the username, when getting user with queries, then usermodel returned")
	public void getUserWithQueries() throws Exception  {
		
		FindUserQueriesModel testModel = new FindUserQueriesModel();
		testModel.setUsername(TEST_USERNAME);
		
		UserModel testUserModel = new UserModel();
		testUserModel.setUsername(TEST_USERNAME);
		
		when(mockedUserService.getUserWithQueries(any(), any(), any(), any(), any()))
				.thenReturn(testUserModel);
		
		
		MvcResult result = mockedMvc.perform(
											post(GET_USER_WITH_QUERIES_URI)
											.accept(MediaType.APPLICATION_JSON)
											.content(mapper.writeValueAsString(testModel))
											.contentType(MediaType.APPLICATION_JSON)
										)
										.andExpect(status().is2xxSuccessful())
										.andReturn()
										;
		
		String content = result.getResponse().getContentAsString();
		
		UserModel body = mapper.readValue(content, UserModel.class);
		
		assertThat(body.getUsername())
					.isEqualTo(TEST_USERNAME);
	}

}
