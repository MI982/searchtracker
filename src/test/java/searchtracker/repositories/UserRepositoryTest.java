package searchtracker.repositories;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import searchtracker.entities.Query;
import searchtracker.entities.Role;
import searchtracker.entities.User;
import searchtracker.entities.UserRole;
import searchtracker.specification.UserSpecification;

@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository testUserRepo;
	
	@Autowired
	private RoleRepository testRoleRepository;
	
	@Autowired
	private QueryRepository testQueryRepository;
	
	private User testUser;
	private UserRole testUserRole;
	private Role testRole;
	private Query testQuery;
	
	final String TEST_USERNAME = "testusername"; 
	final String WRONG_TEST_USERNAME = "wrongtestusername";
	final String TEST_USER_ROLE = "testuserrole";
	final String WRONG_TEST_USER_ROLE = "wrongtestuserrole";
	final String TEST_QUERY_INPUT = "testqueryinput";
	
	@BeforeEach
	public void setupTestUser() {
		this.testUser = new User();
		this.testUser.setUsername(TEST_USERNAME);
		
		this.testUser = testUserRepo.saveAndFlush(testUser);
		
		this.testRole = new Role();
		this.testRole.setRole(TEST_USER_ROLE);
		
		this.testUserRole = new UserRole();
		
		this.testQuery = new Query();
	}
	
	
	@Test
	@DisplayName("Check if username exists, returns true")
	public void existsByUsername() {
		
		assertThat(testUserRepo.existsByUsername(TEST_USERNAME)).isTrue();
		
	}
	
	@Test
	@DisplayName("Check if username exists, returns false")
	public void usernameNotFound(){
		
		assertThat(testUserRepo.existsByUsername(WRONG_TEST_USERNAME)).isFalse();
	}
	
	@Test
	@DisplayName("Find by username, returns the right user")
	public void findByUsername() {
		
		assertThat(testUserRepo.findByUsername(TEST_USERNAME)).isEqualTo(testUser);
	}
	
	@Test
	@DisplayName("Find by non-existant username, return null")
	public void findWrongUser() {
		
		assertThat(testUserRepo.findByUsername(WRONG_TEST_USERNAME)).isNull();
	}
	
	@Test
	@DisplayName("Find by UserSpecification.usernameEquals, returns the right user")
	public void specificationUsernameEquals() {
		
		assertThat(testUserRepo.findOne(UserSpecification.usernameEquals(TEST_USERNAME))).isEqualTo(Optional.of(testUser));
	}
	
	@Test
	@DisplayName("Find by UserSpecification.usernameEquals, doesn't find the user")
	public void specificationUsernameEqualsIsNull() {
		
		assertThat(testUserRepo.findOne(UserSpecification.usernameEquals(WRONG_TEST_USERNAME))).isEqualTo(Optional.empty());
	}
	
	@Test
	@DisplayName("Find by UserSpecification.usernameEquals, finds more than one, throws IncorrectResultSizeDataAccessException")
	public void specificationUsernameEqualsThrowsException() {
		
		User illegalUser = new User();
		illegalUser.setUsername(TEST_USERNAME);
		
		testUserRepo.save(illegalUser);
		
		assertThatExceptionOfType(IncorrectResultSizeDataAccessException.class)
				.isThrownBy(()-> testUserRepo.findOne(UserSpecification.usernameEquals(TEST_USERNAME)));
	}
	
	@Test
	@DisplayName("Find by username and fetch Roles, user has no roles, returns null")
	public void findByUsernameFetchRolesIsNull() {
		
		assertThat(testUserRepo.findOne(UserSpecification.fetchRolesWithUsernameEquals(TEST_USERNAME)))
			.isEqualTo(Optional.empty());
	}
	
	@Test
	@DisplayName("Find by username and fetch Roles, returns user with one role")
	public void findByUsernameFetchRoles() {
		
		testRole = testRoleRepository.saveAndFlush(testRole);
		
		testUserRole.setRole(testRole);
		testUserRole.setUser(testUser);
		
		List<UserRole> testUserRoleList = new ArrayList<UserRole>();
		testUserRoleList.add(testUserRole);
		
		testUser.setUserRoles(testUserRoleList);
		testUserRepo.save(testUser);
		
		assertThat(testUserRepo.findOne(UserSpecification.fetchRolesWithUsernameEquals(TEST_USERNAME))
					.get().getUserRoles().get(0)
					)
			.isEqualTo(testUserRole);
	}
	
	@Test
	@DisplayName("Find user by role name, returns the user with the right role")
	public void findUserByRole() {
		
		testRole = testRoleRepository.saveAndFlush(testRole);
		
		testUserRole.setRole(testRole);
		testUserRole.setUser(testUser);
		
		List<UserRole> testUserRoleList = new ArrayList<UserRole>();
		testUserRoleList.add(testUserRole);
		
		testUser.setUserRoles(testUserRoleList);
		testUserRepo.save(testUser);
		
		assertThat(testUserRepo.findOne(UserSpecification.userRoleIs(TEST_USER_ROLE))
					.get().getUserRoles().get(0).getRole().getRole()// ...what was I thinking...
					)
			.isEqualTo(testRole.getRole());
	}
	
	@Test
	@DisplayName("Find user by role name, doesn't find the user with the right role, returns null")
	public void findUserByRoleReturnsNull() {
		
		testRole = testRoleRepository.saveAndFlush(testRole);
		
		testUserRole.setRole(testRole);
		testUserRole.setUser(testUser);
		
		List<UserRole> testUserRoleList = new ArrayList<UserRole>();
		testUserRoleList.add(testUserRole);
		
		testUser.setUserRoles(testUserRoleList);
		testUserRepo.save(testUser);
		
		assertThat(testUserRepo.findOne(UserSpecification.userRoleIs(WRONG_TEST_USER_ROLE)))
				.isEqualTo(Optional.empty());
	}
	
	@Test
	@DisplayName("Fetch Queries for username, user has no queries, getQueries returns null")
	public void fetchQueriesForUsernameFails() {
		
		assertThat(testUserRepo.findOne(UserSpecification.fetchQueriesFor(TEST_USERNAME))
					.get().getQueries()
				)
				.isNull();
	}
	
	@Test
	@DisplayName("Fetch Queries for username, fetched queries match")
	public void fetchQueriesForUsername() {
		
		testQuery.setInput(TEST_QUERY_INPUT);
		testQuery.setUser(testUser);
		
		testUser.setQueries(Arrays.asList(testQuery));
		
		testQueryRepository.save(testQuery);
		
		assertThat(testUserRepo.findOne(UserSpecification.fetchQueriesFor(TEST_USERNAME))
					.get()
					.getQueries()
					.get(0)
				)
				.isEqualTo(testQuery);
	}

}
