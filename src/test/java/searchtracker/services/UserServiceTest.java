package searchtracker.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import searchtracker.entities.Query;
import searchtracker.entities.Role;
import searchtracker.entities.User;
import searchtracker.entities.UserRole;
import searchtracker.exceptions.UserHasNoQueriesException;
import searchtracker.models.RegisterUserModel;
import searchtracker.repositories.QueryRepository;
import searchtracker.repositories.UserRepository;
import searchtracker.specification.QuerySpecification;
import searchtracker.specification.UserSpecification;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
	
	
	
	final String TEST_USERNAME = "testusername"; 
	final String TEST_USER_PASSWORD = "testuserpassword";
	final String WRONG_TEST_USERNAME = "wrongtestusername";
	final String TEST_USER_ROLE = "testuserrole";
	final String TEST_ADMIN_ROLE = "testadminrole";
	final String WRONG_TEST_USER_ROLE = "wrongtestuserrole";
	final String TEST_QUERY = "testquery";
	
	@Mock
	private UserRepository mockedUserRepository;
	
	@Mock
	private QueryRepository mockedQueryRepository;
	
	@Mock
	private RoleService mockedRoleService;
	
	@Mock
	private PageRequest mockedPageRequest;
	
	@Mock
	private BCryptPasswordEncoder mockedEncoder;
	
	@InjectMocks
	private UserService userServiceTest;
	
	@Test
	@DisplayName("UserService.loadByUsername didn't find the user, UsernameNotFoundException is thrown")
	public void loadByUsernameThrowsException() {
		
		when(mockedUserRepository.findOne(UserSpecification.fetchRolesWithUsernameEquals(any())))
			.thenReturn(Optional.ofNullable(null));
		
		assertThatExceptionOfType(UsernameNotFoundException.class)
				.isThrownBy(()-> userServiceTest.loadUserByUsername(TEST_USERNAME));
	}
	
	@Test
	@DisplayName("UserService.loadByUsername found the correct user")
	public void loadByUsernameReturnsTheRightUser() {
		
		User testUser = new User();
		Role testRole = new Role();
		UserRole testUserRole = new UserRole();
		
		testUserRole.setUser(testUser);
		testUserRole.setRole(testRole);
		
		testUser.setUsername(TEST_USERNAME);
		testUser.setUserRoles(Arrays.asList(testUserRole));
		
		testRole.setRole(TEST_USER_ROLE);
		
		when(mockedUserRepository.findOne(UserSpecification.fetchRolesWithUsernameEquals(any())))
			.thenReturn(Optional.ofNullable(testUser));
		
		assertThat(userServiceTest.loadUserByUsername(TEST_USERNAME)
					.getUsername()
				  )
			.isEqualTo(TEST_USERNAME);
	}
	
	@Test
	@DisplayName("Save query by username, UsernameNotFoundException is thrown")
	public void saveQueryThrowsException() {
		
		when(mockedUserRepository.findOne(UserSpecification.fetchRolesWithUsernameEquals(any())))
		.thenReturn(Optional.ofNullable(null));
		
		assertThatExceptionOfType(UsernameNotFoundException.class)
		.isThrownBy(()-> userServiceTest.saveQuery(TEST_QUERY, TEST_USERNAME));
	}
	
	@Test
	@DisplayName("Save query by username, query gets saved")
	public void saveQueryGetsSaved() {
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		
		when(mockedUserRepository.findOne(UserSpecification.fetchRolesWithUsernameEquals(any())))
		.thenReturn(Optional.ofNullable(testUser));
		
		userServiceTest.saveQuery(TEST_QUERY, TEST_USERNAME);
		
		ArgumentCaptor<Query> queryCaptor = ArgumentCaptor.forClass(Query.class);
		
		verify(mockedQueryRepository).save(queryCaptor.capture());
		
		assertThat(queryCaptor.getValue().getInput())
			.isEqualTo(TEST_QUERY);
		
	}
	
	@Test
	@DisplayName("Save query by username, query gets saved to the correct user")
	public void saveQueryCorrectUser() {
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		
		when(mockedUserRepository.findOne(UserSpecification.fetchRolesWithUsernameEquals(any())))
		.thenReturn(Optional.ofNullable(testUser));
		
		userServiceTest.saveQuery(TEST_QUERY, TEST_USERNAME);
		
		ArgumentCaptor<Query> queryCaptor = ArgumentCaptor.forClass(Query.class);
		
		verify(mockedQueryRepository).save(queryCaptor.capture());
		
		assertThat(queryCaptor.getValue().getUser())
			.isEqualTo(testUser);
	}
	
	@Test
	@DisplayName("Get user queries by username, UserHasNoQueriesException is thrown")
	public void getUserWithQueriesThrows() {
		
		
		Page<Query> testPagedQuery = Page.empty();
		
		when(mockedQueryRepository.findAll(QuerySpecification.getQueryWhereUsername(any())
										, any(PageRequest.class))
			)
			.thenReturn(testPagedQuery);
				
		assertThatExceptionOfType(UserHasNoQueriesException.class)
			.isThrownBy(()-> userServiceTest.getUserWithQueries(TEST_USERNAME, null, null, null, null));
		
	}
	
	@Test
	@DisplayName("Get user queries by username, return first page")
	public void getUserWithQueriesFirstPage() throws UserHasNoQueriesException {
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		
		List<Query> testQueryList = new ArrayList<>();
		
		for(int i = 0; i<10; i++) {
			testQueryList.add(new Query(TEST_QUERY+i, Instant.now().toEpochMilli(), testUser));
		}
		
		Page<Query> testPagedQuery = new PageImpl<Query>(testQueryList, PageRequest.of(1, 5), 10L);
		
		when(mockedQueryRepository.findAll(QuerySpecification.getQueryWhereUsername(any())
										, any(PageRequest.class))
			)
			.thenReturn(testPagedQuery);
				
		assertThat(userServiceTest.getUserWithQueries(TEST_USERNAME, null, null, null, null)
					.getQueryList().getPage()
					)
			.isEqualTo(2);
		
	}
	
	@Test
	@DisplayName("Get user queries by username, return total found")
	public void getUserWithQueriesTotalFound() throws UserHasNoQueriesException {
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		
		List<Query> testQueryList = new ArrayList<>();
		
		for(int i = 0; i<10; i++) {
			testQueryList.add(new Query(TEST_QUERY+i, Instant.now().toEpochMilli(), testUser));
		}
		
		Page<Query> testPagedQuery = new PageImpl<Query>(testQueryList, PageRequest.of(1, 5), 10L);
		
		when(mockedQueryRepository.findAll(QuerySpecification.getQueryWhereUsername(any())
										, any(PageRequest.class))
			)
			.thenReturn(testPagedQuery);
				
		assertThat(userServiceTest.getUserWithQueries(TEST_USERNAME, null, null, null, null)
					.getQueryList().getTotalQueries()
					)
			.isEqualTo(10L);
		
	}
	
	@Test
	@DisplayName("Get user queries by username, return total pages found")
	public void getUserWithQueriesTotalPages() throws UserHasNoQueriesException {
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		
		List<Query> testQueryList = new ArrayList<>();
		
		for(int i = 0; i<10; i++) {
			testQueryList.add(new Query(TEST_QUERY+i, Instant.now().toEpochMilli(), testUser));
		}
		
		Page<Query> testPagedQuery = new PageImpl<Query>(testQueryList, PageRequest.of(1, 5), 10L);
		
		when(mockedQueryRepository.findAll(QuerySpecification.getQueryWhereUsername(any())
										, any(PageRequest.class))
			)
			.thenReturn(testPagedQuery);
				
		assertThat(userServiceTest.getUserWithQueries(TEST_USERNAME, null, null, null, null)
					.getQueryList().getTotalPages()
					)
			.isEqualTo(2);
		
	}
	
	@Test
	@DisplayName("Successfully register user")
	public void registerUser() {
		
		Role testUserRole = new Role();
		testUserRole.setRole(TEST_USER_ROLE);
		
		UserRole testUserrole = new UserRole();
		testUserrole.setRole(testUserRole);
		
		User testUser = new User();
		testUser.setUsername(TEST_USERNAME);
		testUser.setPassword(TEST_USER_PASSWORD);
		testUser.setUserRoles(Arrays.asList(testUserrole));
		
		RegisterUserModel registerUserModelTest = new RegisterUserModel(TEST_USERNAME, TEST_USER_PASSWORD);
		
		when(mockedRoleService.getUserRole()).thenReturn(testUserRole);
		when(mockedEncoder.encode(TEST_USER_PASSWORD)).thenReturn(TEST_USER_PASSWORD);
		when(mockedUserRepository.saveAndFlush(any())).thenReturn(testUser);
		
		
		assertThat(userServiceTest.registerUser(registerUserModelTest, false).getUsername())
				.isEqualTo(TEST_USERNAME);
		
		verify(mockedUserRepository, times(1)).saveAndFlush(any());
	}

}
