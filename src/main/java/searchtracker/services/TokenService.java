package searchtracker.services;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;



@Service 
public class TokenService {
	
	private String tokenSecret = "ThisIsNotAGoodSolution";
	private final String AUTHORITIES = "Authorities";
	private int tokenDuration = 30;

	
	public String createJwToken(String username, List<? extends GrantedAuthority> grantedAuthorities) {
		return Jwts.builder()
				 .setSubject(username)
				 .claim(AUTHORITIES, grantedAuthorities)
				 .setIssuer("SearchTracker") // not sure what to put here
				 .setIssuedAt(new Date(System.currentTimeMillis()))
				 .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(tokenDuration))))
				 .signWith(SignatureAlgorithm.HS256, tokenSecret)
				 .compact();
	}
	
	private Claims getAllClaims(String token) 
			throws ExpiredJwtException, UnsupportedJwtException, 
					MalformedJwtException, SignatureException, IllegalArgumentException
	{
		return Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();
	}
	
	public String getUsername(String token) {
		return getAllClaims(token).getSubject();
	}
	
	@SuppressWarnings("unchecked")
	public List<LinkedHashMap<String, String>> getGrantedAuthorities(String token){
		return (List<LinkedHashMap<String, String>>)getAllClaims(token).get(AUTHORITIES);
	}
	
	public Boolean hasTokenExpired(String token) {
		return getAllClaims(token).getExpiration().after(Date.from(Instant.now()));
	}
}
