package searchtracker.services;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import searchtracker.entities.Query;
import searchtracker.entities.Role;
import searchtracker.entities.User;
import searchtracker.entities.UserRole;
import searchtracker.exceptions.UserHasNoQueriesException;
import searchtracker.models.QueryListModel;
import searchtracker.models.QueryModel;
import searchtracker.models.RegisterUserModel;
import searchtracker.models.UserModel;
import searchtracker.repositories.QueryRepository;
import searchtracker.repositories.UserRepository;
import searchtracker.specification.QuerySpecification;
import searchtracker.specification.UserSpecification;
import searchtracker.entities.Query_;

@Service
@Slf4j
public class UserService implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private QueryRepository queryRepo;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	

	@Override
	public UserModel loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepo.findOne(UserSpecification.fetchRolesWithUsernameEquals(username))
										.orElseThrow(()-> new UsernameNotFoundException(username));
		
		UserModel userModel = new UserModel(user);
		userModel.setAuthorities(user.getUserRoles().stream()
									.map(userRole -> userRole.getRole().getRole()).collect(Collectors.toList()));
		return userModel;
	}
	
	
	
	public boolean usernameExists(String username) {
		return userRepo.existsByUsername(username);
	}
	
	
	public void saveQuery(String searchQuery, String username) throws UsernameNotFoundException {
		User user = userRepo.findOne(UserSpecification.fetchQueriesFor(username))
								.orElseThrow(()-> new UsernameNotFoundException(username));
		
		Query query = new Query(searchQuery, Instant.now().toEpochMilli(), user);
		
		queryRepo.save(query);
		
	}
	
	public UserModel getUserWithQueries(String username, Integer page, Integer perPage, String sortBy, String direction) throws UserHasNoQueriesException {
		
		page = page==null || page==0 ? 0:page-1;
		perPage = perPage==null || perPage<5? 5:perPage;
		sortBy = sortBy==null? Query_.TIME: sortBy;
		Direction sortDirection = direction == null 
									||Strings.isNotEmpty(direction) 
									|| direction.equalsIgnoreCase(Direction.DESC.name())?
									Direction.DESC : Direction.ASC;
		
		Sort sort = Sort.by(sortDirection, sortBy);
		
		PageRequest pageRequest = PageRequest.of(page, perPage, sort);
		
		Page<Query> queryPage = queryRepo.findAll(QuerySpecification.getQueryWhereUsername(username), 
													pageRequest
													);
		
		List<QueryModel> queries = new ArrayList<>();
		
		if(queryPage.getContent().isEmpty()) throw new UserHasNoQueriesException(username);
		
		queryPage.getContent().forEach(query -> queries.add(new QueryModel(query)));
		
		User user = queryPage.getContent().get(0).getUser(); //userRepo.findByUsername(username); -> it's already fetching the user
		
		UserModel userModel = new UserModel(user);
		
		userModel.setQueryList(
					new QueryListModel(
							queryPage.getNumber()+1, queryPage.getTotalPages(), queryPage.getTotalElements(), queries
							)
					);
		
		return userModel;
	}

	
	@Transactional
	public UserModel registerUser(RegisterUserModel register, Boolean isAdmin) {
		
		User user = new User();
		UserRole userRole = new UserRole();// remember to change this relation!!!!
		
		Role role = isAdmin ? roleService.getAdminRole(): roleService.getUserRole();
		userRole.setRole(role);
		userRole.setUser(user);
		user.setUsername(register.getUsername());
		user.setPassword(passwordEncoder.encode(register.getPassword()));
		user.setIsLoggedOut(0);
		user.setUserRoles(Arrays.asList(userRole));
		
		userRepo.saveAndFlush(user);
		
		UserModel userModel = new UserModel(user);
		userModel.setAuthorities(Arrays.asList(role.getRole()));
		
		return userModel;
		
		
	}



	public List<UserModel> getAllUsers() {// implement search and pagination
		// returns admin as well, needs to be changed
		List<UserModel> users = userRepo.findAll(Specification.where(UserSpecification.userRoleIs("ROLE_USER")))
											.stream().map(UserModel::new).collect(Collectors.toList());
		return users;
	}
	
	

}
