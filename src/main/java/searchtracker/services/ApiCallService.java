package searchtracker.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;
import searchtracker.models.ApiCallResponseModel;

@Service
public class ApiCallService {

	
	public Mono<ApiCallResponseModel> callAndReturn(String uri, String search) {
		
		return  WebClient.create(uri+"/"+search)
							.get()
							.header("apikey", "12345")
							.accept(MediaType.APPLICATION_JSON)
							.retrieve()
							.bodyToMono(ApiCallResponseModel.class)
							;
	}
	
	public Mono<ApiCallResponseModel> testMono(String query){
		List<String> payload = Arrays.asList("1","2","3","4", query);
		return Mono.just(new ApiCallResponseModel(payload));
	}
}
