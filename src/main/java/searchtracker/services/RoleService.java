package searchtracker.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import searchtracker.entities.Role;
import searchtracker.exceptions.UserRoleNotFoundException;
import searchtracker.repositories.RoleRepository;

@Service
public class RoleService {

	@Autowired
	private RoleRepository roleRepo;
	
	public Role saveUserRole(String authority) {
		return roleRepo.saveAndFlush(new Role(authority, null));
	}
	
	public Role getUserRole() {
		return roleRepo.getByRole("ROLE_USER").orElseThrow(()-> new UserRoleNotFoundException());
	}
	
	public Role getAdminRole() {
		return roleRepo.getByRole("ROLE_ADMIN").orElseThrow(()-> new UserRoleNotFoundException());
	}
	
	public List<Role> getAllRoles(){
		return roleRepo.findAll();
	}
}
