package searchtracker.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import searchtracker.models.UserModel;
import searchtracker.services.TokenService;
import searchtracker.services.UserService;

@Data
@Component
@Slf4j
public class AuthorizationFilter extends OncePerRequestFilter{
	
	private final String BEARER = "Bearer "; 
	private final String AUTHORIZATION = "Authorization";
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenService tokenService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		UserModel user = getUserModelFrom(request);
		
		if (user != null) {
			SecurityContextHolder.getContext()
					.setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
		}
		filterChain.doFilter(request, response);
		
	}

	private UserModel getUserModelFrom(HttpServletRequest request) throws ServletException {
		
		UserModel user = null;
		
		String header = request.getHeader(AUTHORIZATION);
		
		
		if(header != null && !header.isBlank() && header.startsWith(BEARER)) {
			
			try {
				String token = header.replace(BEARER, "");
				user = new UserModel();
				user.setUsername(tokenService.getUsername(token));
				
				user.setAuthorities(
						tokenService.getGrantedAuthorities(token).stream()
							.map(ga -> ga.get("authority")).collect(Collectors.toList())
					);
				
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		return user;
	}
	
	

}
