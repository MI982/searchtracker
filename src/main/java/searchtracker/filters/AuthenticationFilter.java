package searchtracker.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import searchtracker.exceptions.AuthenticationFailedException;
import searchtracker.models.LoginUserModel;
import searchtracker.validators.LoginUserValidator;

@Slf4j
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	@Bean
	private RedirectStrategy getRedirectStrategy() {
		return new DefaultRedirectStrategy();
	}
	
	@Autowired
	private LoginUserValidator validator;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		LoginUserModel user = getLoginUserModelFrom(request);
		DataBinder binder = new DataBinder(user);
		binder.addValidators(validator);
		binder.validate();
		
		BindingResult result = binder.getBindingResult();
		
		if(result.hasErrors()) throw new AuthenticationFailedException(result);
		
		return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
	}
	
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		
		SecurityContextHolder.getContext().setAuthentication(authResult);
		
		request = new HttpServletRequestWrapper((HttpServletRequest) request) {
			
			@Override
			public String getRequestURI() {
				return "/user/returntoken";
			}
			
			@Override
			public StringBuffer getRequestURL() {
				return new StringBuffer("/user/returntoken");
			}
			
			@Override
			public String getMethod() {
				return "GET";
			}
		};
		
		chain.doFilter(request, response);
	}



	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		throw new ServletException(failed);
	}
	
    
	
	private LoginUserModel getLoginUserModelFrom(HttpServletRequest request) {
		
		try {
			return  new ObjectMapper().readValue(request.getInputStream(), LoginUserModel.class);
			
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new AuthenticationServiceException("Object mapper failed!", e);
		}
	}


}
