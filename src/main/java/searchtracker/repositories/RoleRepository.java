package searchtracker.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import searchtracker.entities.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>{

	Optional<Role> getByRole(String string);
	
	
}
