package searchtracker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import searchtracker.entities.Query;

public interface QueryRepository extends JpaRepository<Query, Integer>, JpaSpecificationExecutor<Query> {

}
