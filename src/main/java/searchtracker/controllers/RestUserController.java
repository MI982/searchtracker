package searchtracker.controllers;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import searchtracker.exceptions.UserHasNoQueriesException;
import searchtracker.exceptions.UsernameExistsException;
import searchtracker.models.FindUserQueriesModel;
import searchtracker.models.RegisterUserModel;
import searchtracker.models.UserModel;
import searchtracker.services.TokenService;
import searchtracker.services.UserService;


@RestController
@RequestMapping("user")
@Slf4j
public class RestUserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenService tokenService;
	
	@GetMapping(value = "/securitytest1", produces = "application/json")
	public ResponseEntity<?> securityTest(){
		HashMap<String, String> body = new HashMap<String, String>();
		body.put("message", "Security 1 operational!");
		return ResponseEntity.ok(body);
	}
	
	@GetMapping(value = "/securitytest2")
	public ResponseEntity<?> securityTestTwo(){
		HashMap<String, String> body = new HashMap<String, String>();
		body.put("message", "Security 2 operational!");
		return ResponseEntity.ok(body);
	}
	
	@GetMapping(value = "/securitytest3")
	public ResponseEntity<?> securityTestThree(){
		HashMap<String, String> body = new HashMap<String, String>();
		body.put("message", "Security 3 operational!");
		return ResponseEntity.ok(body);
	}
	
	@GetMapping(value = "/allusers")
	public ResponseEntity<?> getAllUsers(){
		List<UserModel> users = userService.getAllUsers();
		
		return ResponseEntity.ok(users);
	}
	
	@PostMapping(value = "/finduser")
	public ResponseEntity<?> getSingleUser(@RequestBody FindUserQueriesModel findUserQueriesModel) 
			throws UserHasNoQueriesException
	{
		
		String username = findUserQueriesModel.getUsername();
		Integer page = findUserQueriesModel.getPage();
		Integer perPage = findUserQueriesModel.getPerPage();
		String sortBy = findUserQueriesModel.getSortBy();
		String direction = findUserQueriesModel.getDirection();
		
		return ResponseEntity.ok(userService.getUserWithQueries(username, page, perPage, sortBy, direction));
	}
	
	@PostMapping("/registeruser")
	public ResponseEntity<?> registerUser(@RequestBody RegisterUserModel register){
		if(userService.usernameExists(register.getUsername())) throw new UsernameExistsException(register.getUsername());
		
		UserModel user = userService.registerUser(register, false);
		
		String token = tokenService.createJwToken(user.getUsername(), user.getAuthorities());
		
		HashMap<String, String> body = new HashMap<>();
		
		body.put("jtw", token);
		
		return ResponseEntity.ok(body);
	}
	
	@GetMapping("/returntoken")
	public ResponseEntity<?> returnToken(Authentication authentication){
		
		UserModel user = (UserModel)authentication.getPrincipal();
		
		HashMap<String, String> payload = new HashMap<>();
		payload.put("jwt", tokenService.createJwToken(user.getUsername(), user.getAuthorities()));
		
		return ResponseEntity.ok(payload);
	}

}
