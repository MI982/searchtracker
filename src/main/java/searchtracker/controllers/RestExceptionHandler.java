package searchtracker.controllers;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import searchtracker.exceptions.AuthenticationFailedException;
import searchtracker.exceptions.UserHasNoQueriesException;

@RestControllerAdvice
public class RestExceptionHandler implements ErrorController {
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public ResponseEntity<?> noHandlerFound(NoHandlerFoundException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("error", exception.getMessage());
		
		return ResponseEntity.badRequest().body(body);
	}
	
	@ExceptionHandler(UserHasNoQueriesException.class)
	public ResponseEntity<?> userHasNoQueries(UserHasNoQueriesException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("message", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
	}
	
	@ExceptionHandler(ExpiredJwtException.class)
	public ResponseEntity<?> expiredJwtException(ExpiredJwtException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("message", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
	}
	
	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<?> badCredentialsException(BadCredentialsException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("Error", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
	}
	
	@ExceptionHandler(AuthenticationFailedException.class)
	public ResponseEntity<?> authenticationFailedException(AuthenticationFailedException exception, HttpServletResponse response){
		HashMap<String, String> body = this.getHashMapFromBindingResult(exception.getResult());
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
	}
	
	@ExceptionHandler(SignatureException.class)
	public ResponseEntity<?> signatureException(SignatureException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("Error", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
	}
	
	@ExceptionHandler(MalformedJwtException.class)
	public ResponseEntity<?> malformedJwtException(MalformedJwtException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("Error", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
	}
	
	@ExceptionHandler(PropertyReferenceException.class)
	public ResponseEntity<?> propertyReferenceException(PropertyReferenceException exception, HttpServletResponse response){
		HashMap<String, String> body = new HashMap<>();
		
		body.put("Error", exception.getMessage());
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
	}
	
	
	
	//helper method
	private HashMap<String, String> getHashMapFromBindingResult(BindingResult bindingResult){
		HashMap<String, String> errorHolder = new HashMap<>();
		bindingResult.getAllErrors().forEach(error -> {
			String fieldName = ((FieldError)error).getField();
			String errorMessage = error.getDefaultMessage();
			if(Strings.isNotBlank(errorMessage)) errorHolder.put(fieldName, errorMessage);
		});
		return errorHolder;
	}

}
