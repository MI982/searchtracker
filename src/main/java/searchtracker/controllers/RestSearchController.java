package searchtracker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import searchtracker.models.ApiCallResponseModel;
import searchtracker.models.SearchModel;
import searchtracker.models.UserModel;
import searchtracker.services.ApiCallService;
import searchtracker.services.UserService;

@RestController("/search")
public class RestSearchController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ApiCallService callService;

	@GetMapping("/query")
	public Mono<ApiCallResponseModel> query(@RequestBody SearchModel searchModel, 
									Authentication authentication, 
									BindingResult bindingResult)
									throws MethodArgumentNotValidException 
	{
		if(bindingResult.hasErrors()) throw new MethodArgumentNotValidException(null, bindingResult);
		
		UserModel user = (UserModel) authentication.getPrincipal();
		
		userService.saveQuery(searchModel.getQuery(), user.getUsername());
		
		// implement api call
//		return callService.callAndReturn("localhost:8081/search", 
//																				searchModel.getQuery()
//																			  ); 
		return callService.testMono(searchModel.getQuery());
	}
}
