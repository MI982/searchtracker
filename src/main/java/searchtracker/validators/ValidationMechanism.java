package searchtracker.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ValidationMechanism implements ConstraintValidator<ValidateBy, Object>  {
	
	@Autowired
	private ApplicationContext appContext;
	
	private Validator validator;
	private Errors errors;
	private boolean isValid;

	@Override
	public void initialize(ValidateBy validateBy) {
		validator = appContext.getBean(validateBy.validator());
		errors = appContext.getBean(Errors.class);
		isValid = true;
	}

	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		
		validator.validate(object, errors);
		
		if(errors.hasErrors()) {
			this.isValid = false;
			errors.getFieldErrors().forEach(error -> {
				context.buildConstraintViolationWithTemplate(error.getDefaultMessage())
						.addPropertyNode(error.getField())
						.addConstraintViolation();
			});
		}
		
		return this.isValid;
	}

}
