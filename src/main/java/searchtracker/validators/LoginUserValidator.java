package searchtracker.validators;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import searchtracker.exceptions.InvalidValidationObject;
import searchtracker.models.LoginUserModel;

@Component
@Data
@Slf4j
public class LoginUserValidator implements Validator {

	
	private String message;
	
	private LoginUserModel loginUserModel;
	
	private final String USERNAME = "username";
	private final String PASSWORD = "password";
	private final String USERNAMEERRORMESSAGE = "Username must be provided!";
	private final String PASSWORDERRORMESSAGE = "Password must be provided!";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return LoginUserModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) throws InvalidValidationObject{
		
		if(this.supports(object.getClass())) {
			
			loginUserModel = (LoginUserModel) object;
			
			if(usernameIsNotValid()) errors.rejectValue(USERNAME, null, USERNAMEERRORMESSAGE);
			
			if(passwordIsNotValid()) errors.rejectValue(PASSWORD, null, PASSWORDERRORMESSAGE);
			
		} else {
			throw new InvalidValidationObject(LoginUserModel.class.getName(), object.getClass().getName());
		}
		
	}
	
	private boolean usernameIsNotValid() {
		return Strings.isBlank(loginUserModel.getUsername());
	}
	
	private boolean passwordIsNotValid() {
		return Strings.isBlank(loginUserModel.getPassword());
	}

}
