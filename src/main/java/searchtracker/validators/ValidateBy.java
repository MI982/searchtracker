package searchtracker.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


import javax.validation.Constraint;
import javax.validation.Payload;

import org.springframework.validation.Validator;


@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = { ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Constraint(validatedBy = { ValidationMechanism.class })
public @interface ValidateBy {
	
	Class<? extends Validator> validator();
	
	String message() default "Not valid";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
