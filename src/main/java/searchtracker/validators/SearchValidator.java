package searchtracker.validators;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lombok.Data;
import searchtracker.exceptions.InvalidValidationObject;
import searchtracker.models.SearchModel;

@Data
@Component
public class SearchValidator implements Validator {
	
	private SearchModel searchModel;
	
	private final String QUERY = "query";
	private final String ERRORMESSAGE = "Query must not be empty!";

	@Override
	public boolean supports(Class<?> clazz) {
		return SearchModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) throws InvalidValidationObject {
		
		if(this.supports(target.getClass())) {
			
			searchModel = (SearchModel) target;
			
			if(Strings.isBlank(searchModel.getQuery())) errors.rejectValue(QUERY, null, ERRORMESSAGE);
		} else {
			throw new InvalidValidationObject(SearchModel.class.getName(), target.getClass().getName());
		}

	}

}
