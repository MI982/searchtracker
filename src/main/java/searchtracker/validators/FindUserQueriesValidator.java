package searchtracker.validators;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import searchtracker.exceptions.InvalidValidationObject;
import searchtracker.models.FindUserQueriesModel;
import searchtracker.models.LoginUserModel;
import searchtracker.services.UserService;

public class FindUserQueriesValidator implements Validator {

	
	private final String USERNAME = "username";
	
	private final String ERRORMESSAGE = "Valid username must be provided!";
	
	@Autowired
	private FindUserQueriesModel findUserQueriesModel;
	
	@Autowired
	private UserService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return FindUserQueriesModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		if(this.supports(target.getClass())) { 
			findUserQueriesModel = (FindUserQueriesModel) target; 
			
			if(usernameIsNotValid()) errors.rejectValue(USERNAME, null, ERRORMESSAGE);
			
		} else throw new InvalidValidationObject(FindUserQueriesModel.class.getName(), target.getClass().getName());

	}
	
	private boolean usernameIsNotValid() {
		String username = findUserQueriesModel.getUsername();
		
		return Strings.isBlank(username) || !userService.usernameExists(username);
	}

}
