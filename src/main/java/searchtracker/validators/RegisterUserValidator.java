package searchtracker.validators;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import searchtracker.models.RegisterUserModel;

@Component
public class RegisterUserValidator implements Validator{
	
	private final String USERNAME = "username";
	private final String PASSWORD = "password";
	private final String USERNAMEERRORMESSAGE = "Username must be provided!";
	private final String PASSWORDERRORMESSAGE = "Password must be provided!";
	
	private RegisterUserModel registerUserModel;

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterUserModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(this.supports(target.getClass())) {
			
			registerUserModel = (RegisterUserModel) target;
			
			if(usernameIsNotValid()) errors.rejectValue(USERNAME, null, USERNAMEERRORMESSAGE);
			
			if(passwordIsNotValid()) errors.rejectValue(PASSWORD, null, PASSWORDERRORMESSAGE);
		}
		
	}
	
	private boolean usernameIsNotValid() {
		return Strings.isBlank(registerUserModel.getUsername());
	}
	
	private boolean passwordIsNotValid() {
		return Strings.isBlank(registerUserModel.getPassword());
	}

}
