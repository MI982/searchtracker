package searchtracker.exceptions;

import lombok.Data;

@Data

public class InvalidValidationObject extends RuntimeException{
	
	private String message;
	
	public InvalidValidationObject(String expectedObject, String receivedObject) {
		message = new StringBuilder("Expected object: ")
								.append(expectedObject)
								.append(" , recieved object: ")
								.append(receivedObject)
								.append(" !")
								.toString();
		
		
	}

}
