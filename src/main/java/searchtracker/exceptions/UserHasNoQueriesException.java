package searchtracker.exceptions;

import lombok.Data;

@Data
public class UserHasNoQueriesException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private String message ;
	
	public UserHasNoQueriesException() {this.message = "User has no queries";}
	
	public UserHasNoQueriesException(String username) {
		this.message = new StringBuilder(username).append(" has no queries!").toString();
	}
	
	
	

}
