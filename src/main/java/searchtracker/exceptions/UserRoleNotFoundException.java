package searchtracker.exceptions;

import lombok.Data;

@Data
public class UserRoleNotFoundException extends RuntimeException{
	
	private String message = "ROLE_USER not in database!";

}
