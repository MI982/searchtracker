package searchtracker.exceptions;

import lombok.Data;

@Data
public class UsernameExistsException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	private String message;
	
	public UsernameExistsException(String username) {
		this.message = "Username '"+username+"' already exists!";
	}
	

}
