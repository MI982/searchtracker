package searchtracker.exceptions;

import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;

import lombok.Data;

@Data
public class AuthenticationFailedException extends AuthenticationException{
	
	private String message = "Authentication failed!";
	
	private BindingResult result;
	
	public AuthenticationFailedException(String message) {
		super(message);
		this.message = message;
	}
	
	public AuthenticationFailedException(BindingResult result) {
		super(null);
		this.result = result;
	}

}
