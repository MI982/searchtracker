package searchtracker.models;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;
import lombok.NoArgsConstructor;
import searchtracker.entities.Query;
import searchtracker.entities.User;
import searchtracker.entities.UserRole;

@Data
@NoArgsConstructor
public class UserModel implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	
	@JsonIgnore
	private String password;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private int isLoggedOut;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<String> authorities;
	
	private QueryListModel queryList;
	
	
	public UserModel(User user) {
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.isLoggedOut = user.getIsLoggedOut();
		
	}
	
	
	

	@Override
	public List<SimpleGrantedAuthority> getAuthorities() {// this is convoluted
		return this.authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}
	

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@JsonProperty(access = Access.WRITE_ONLY)
	public boolean isEnabled() {
		return this.isLoggedOut<1;
	}

}
