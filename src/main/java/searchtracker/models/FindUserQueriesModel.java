package searchtracker.models;

import lombok.Data;
import searchtracker.validators.FindUserQueriesValidator;
import searchtracker.validators.ValidateBy;

@Data
@ValidateBy(validator = FindUserQueriesValidator.class)
public class FindUserQueriesModel {
	
	private String username;
	
	private Integer page;
	
	private Integer perPage;
	
	private String sortBy;
	
	private String direction;

}
