package searchtracker.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class QueryListModel {
	
	private Integer page;
	
	private Integer totalPages;
	
	private Long totalQueries;
	
	private List<QueryModel> queries;
	
	
	

}
