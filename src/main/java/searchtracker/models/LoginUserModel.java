package searchtracker.models;

import lombok.Data;
import searchtracker.validators.LoginUserValidator;
import searchtracker.validators.ValidateBy;

@Data
@ValidateBy(validator = LoginUserValidator.class)
public class LoginUserModel {
	
	private String username, password;

}
