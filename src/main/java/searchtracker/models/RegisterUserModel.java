package searchtracker.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import searchtracker.validators.RegisterUserValidator;
import searchtracker.validators.ValidateBy;

@Data
@AllArgsConstructor
@ValidateBy(validator = RegisterUserValidator.class)
public class RegisterUserModel {

	private String username;
	private String password;
}
