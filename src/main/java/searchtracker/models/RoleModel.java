package searchtracker.models;

import org.springframework.security.core.GrantedAuthority;

// Don't forget to delete this class
public class RoleModel implements GrantedAuthority {
	
	private String authority;

	@Override
	public String getAuthority() {
		return this.authority;
	}
	
	public void setAuthority(String authority) {
		this.authority = authority;
	}

}
