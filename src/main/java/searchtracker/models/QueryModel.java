package searchtracker.models;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import searchtracker.entities.Query;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryModel {
	
	@JsonIgnore
	private final String dateTimeFormat = "HH:mm:ss dd-MM-uuuu";
	
	private String query;
	private String date;

	public QueryModel(Query query) {
		this.query = query.getInput();
		this.date = LocalDateTime.ofInstant(
						Instant.ofEpochMilli(query.getTime()), 
						ZoneOffset.systemDefault())
						.format(DateTimeFormatter.ofPattern(dateTimeFormat))
					.toString();
	}
}
