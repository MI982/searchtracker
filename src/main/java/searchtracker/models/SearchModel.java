package searchtracker.models;

import lombok.Data;
import searchtracker.validators.ValidateBy;
import searchtracker.validators.SearchValidator;

@Data
@ValidateBy(validator = SearchValidator.class)
public class SearchModel {
	
	private String query;
	
	private Integer page = 1;
	private Integer perPage = 10;
	
	private String sortBy;
	private String sortDirection = "DESC";
	
	private final String ASC = "ASC";
	private final String DESC = "DESC";
	
	public SearchModel (String query) {
		this.query = query;
	}
	
	public SearchModel (String query, Integer page, Integer perPage, String sortBy, String sortDirection) {
		this.query = query;
		this.page = page == null? this.page: page;
		this.perPage = perPage == null? this.perPage: perPage;
		this.sortBy = sortBy;
		this.sortDirection = sortDirection.equalsIgnoreCase(ASC)? ASC:DESC;
	}
}
