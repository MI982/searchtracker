package searchtracker;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import searchtracker.models.RegisterUserModel;
import searchtracker.services.RoleService;
import searchtracker.services.UserService;

@SpringBootApplication
public class SearchTrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchTrackerApplication.class, args);
	}
	
	
	@Bean
	CommandLineRunner run(UserService uService, RoleService rService){
		return args ->{
			
			rService.saveUserRole("ROLE_USER");
			rService.saveUserRole("ROLE_ADMIN");
			
			RegisterUserModel defaultUser = new RegisterUserModel("DefaultUser", "12345");
			RegisterUserModel defaultUser2 = new RegisterUserModel("DefaultUser2", "12345");
			RegisterUserModel admin = new RegisterUserModel("Admin", "admin");
			uService.registerUser(defaultUser, false);
			for(int i = 1; i<=20; i++) {
				uService.saveQuery("DefaultUserQuery"+i, defaultUser.getUsername());
				Thread.sleep(1000);
			}
			uService.registerUser(defaultUser2, false);
			uService.registerUser(admin, true);
			uService.saveQuery("AdminQuery", admin.getUsername());
			
		};
	}

}
