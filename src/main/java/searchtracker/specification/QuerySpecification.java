package searchtracker.specification;

import org.springframework.data.jpa.domain.Specification;

import searchtracker.entities.Query;
import searchtracker.entities.Query_;
import searchtracker.entities.User_;

public final class QuerySpecification {
	
	private QuerySpecification() {}
	
	public static Specification<Query> getQueryWhereUsername(String username) {
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.join(Query_.USER).get(User_.USERNAME), username);
		};
	}

}
