package searchtracker.specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

import org.springframework.data.jpa.domain.Specification;

import searchtracker.entities.Role_;
import searchtracker.entities.User;
import searchtracker.entities.UserRole_;
import searchtracker.entities.User_;

public final class UserSpecification {
	
	private UserSpecification() {}
	
	
	@SuppressWarnings("unchecked")
	public static Specification<User> fetchRolesWithUsernameEquals(String username){
		
		return (root, query, criteriaBuilder) -> {
			( (Join<Object, Object>) root.fetch(User_.USER_ROLES)).fetch(UserRole_.ROLE);
			return criteriaBuilder.equal(root.get(User_.USERNAME), username);
		};
	}
	
	public static Specification<User> fetchQueriesFor(String username){
		
		return(root, query, criteriaBuilder)->{
			root.fetch(User_.QUERIES, JoinType.LEFT);
			return criteriaBuilder.equal(root.get(User_.USERNAME), username);
		};
	}
	
	public static Specification<User> userRoleIs(String userRole){
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.join(User_.USER_ROLES).join(UserRole_.ROLE).get(Role_.ROLE), userRole);
		};
	}
	
	public static Specification<User> usernameEquals(String username){
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.get(User_.USERNAME), username);
		};
	}

}
