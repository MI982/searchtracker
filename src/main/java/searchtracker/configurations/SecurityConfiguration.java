package searchtracker.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import lombok.extern.slf4j.Slf4j;
import searchtracker.filters.AuthenticationFilter;
import searchtracker.filters.AuthorizationFilter;
import searchtracker.filters.MasterFilter;
import searchtracker.services.UserService;

@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
	public AuthenticationFilter authenticationFilterBean() throws Exception {
		AuthenticationFilter filter = new AuthenticationFilter();
		
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
		
		return filter;
	}
	
	@Autowired
	private MasterFilter masterFilter;
	
	@Autowired
	private AuthorizationFilter authorizationFilter;
	
	@Autowired
	private UserService uService;

	
	protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
		authBuilder.userDetailsService(uService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable();
		http.authorizeRequests()
			.antMatchers("/user/securitytest1", "/user/registeruser", "/login").permitAll()
			.antMatchers("/user/securitytest2", "/user/returntoken", "/user/search", "/search/query").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
			.antMatchers("/user/securitytest3", "/user/finduser", "/user/allusers").hasAnyAuthority("ROLE_ADMIN")
			.anyRequest().authenticated()
			.and()
			.addFilterAt(authenticationFilterBean(), UsernamePasswordAuthenticationFilter.class)
			.addFilterAfter(authorizationFilter, AuthenticationFilter.class)
			.addFilterBefore(masterFilter, AuthenticationFilter.class)
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			;
	}


}
