package searchtracker.entities;

import java.io.Serializable;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class UserRoleKey implements Serializable{

	private User user;
	private Role role;
}
