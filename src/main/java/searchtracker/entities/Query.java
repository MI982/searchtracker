package searchtracker.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Query extends EntityTemplate{

	private String input;
	
	private Long time;
	
	@ManyToOne
	private User user;
}
